(defvar *prog-vars* nil)
(defvar *prog-golist* nil)

; Hack to make Futamura's PE run.
; This works because Futamura's PE uses GET to check if these atoms are
; associated with SUBR's or FSUBR's, but it doesnt fetch the actual expression.
(setf (get 'EVAL 'SUBR) T)
(setf (get 'RPLACD 'SUBR) T)
(setf (get 'LIST 'FSUBR) T)
(setf (get 'CONS 'SUBR) T)
(setf (get 'NULL 'SUBR) T)
(setf (get 'EVLIS 'SUBR) T)
(setf (get 'CDR 'SUBR) T)
(setf (get 'NCONC 'SUBR) T)
(setf (get 'APPEND 'SUBR) T)
(setf (get 'ZEROP 'SUBR) T)
(setf (get 'SUB1 'SUBR) T)
(setf (get 'TIMES 'SUBR) T)
(setf (get 'ADD1 'SUBR) T)

(defun read-file-to-list (infile)
  (with-open-file (stream infile :direction :input :if-does-not-exist nil)
    (loop for line = (read-line stream nil)
          while line
          collect line)))

(defun read-file (infile)
  (with-open-file (instream infile :direction :input :if-does-not-exist nil)
    (when instream
      (let ((string (make-string (file-length instream))))
        (read-sequence string instream)
        string))))

(defun evalquote-loop (program)
  ;(format t "~a~%~%" program)
  (cond
    ((null program) nil)
    (t (progn
         (l15-evalquote (caar program) (cdar program))
         (format t "-----------------------------------------------------~%")
         (evalquote-loop (cdr program))))))

(defun l15-evalquote (fn args)
  (format t "FUNCTION EVALQUOTE HAS BEEN ENTERED. ARGUMENTS..~%")
  (format t "~a~%" fn )
  (format t "~a~%~%" args )
  (format t "END OF EVALQUOTE. VALUE IS..~%")
  (cond
    ; Some functions are not implemented as FEXPR or FSUBR
    ; but should be treated as such
    ((and (atom fn) (or (get fn 'FEXPR) (get fn 'FSUBR) (eq fn 'CSETQ)
                        (eq fn 'LIST) (eq fn 'AND) (eq fn 'OR) (eq fn 'PROG)
                        (eq fn 'SETQ)))
     (format t "~a~%~%" (l15-eval (cons fn args) nil)))
    (t (format t "~a~%~%" (l15-apply fn args nil)))))

(defun l15-apply (fn args a)
  (cond
    ((null fn) nil)
    ((atom fn)
     (cond
       ((get fn 'EXPR) (l15-apply (get fn 'EXPR) args a)) ;?
       ; get SUBR
       ((eq fn 'CAR) (car (car args)))
       ((eq fn 'CDR) (cdr (car args)))
       ((eq fn 'CONS) (cons (car args) (cadr args)))
       ((eq fn 'ATOM) (l15-atom (car args)))
       ((eq fn 'NULL) (null (car args)))
       ((eq fn 'NUMBERP) (numberp (car args)))
       ((eq fn 'PRINT) (format t "~%~a~%" (car args)))
       ((eq fn 'PRINTG) (format t "~%G~a: " (car args)))
       ((eq fn 'EQ) (l15-eq (car args) (cadr args)))
       ((eq fn 'EQUAL) (equal (car args) (cadr args)))
       ((eq fn 'DEFINE) (mapcar 'l15-define (car args)))
       ((eq fn 'DEFLIST) (mapcar 'l15-deflist (car args)
                                 (make-list (length (car args))
                                            :initial-element (cadr args))))
       ((eq fn 'EVAL) (l15-eval (car args) (cadr args)))
       ((eq fn 'APPLY) (l15-apply (car args) (cadr args) (caddr args)))
       ((eq fn 'CSET) (l15-cset (car args) (car (cdr args))))
       ((eq fn 'ERROR) (error "(APPLY) ERROR: ~S" (cons fn args) ))
       ((eq fn 'EVLIS) (l15-evlis (car args) (cadr args)))
       ((eq fn 'GET) (get (car args) (cadr args)))
       ((eq fn 'COND) (l15-evcon args a))
       ((eq fn 'NCONC) (l15-nconc (car args) (cadr args)))
       ((eq fn 'RPLACD) (l15-rplacd (car args) (cadr args)))
       ((eq fn 'ZEROP) (zerop (car args)))
       ((eq fn 'TIMES) (* (car args) (cadr args)))
       ((eq fn 'SUB1) (- (car args) 1))
       ((eq fn 'ADD1) (+ (car args) 1))
       (t
         (cond
           ((null (assoc fn a))
            (error "(APPLY) A2: FUNCTION OBJECT (~S) HAS NO DEFINITION" fn))
           (t (l15-apply (cdr (assoc fn a)) args a) )
           )) ))
    ((eq (car fn) 'FUNARG) (l15-apply (cadr fn) args (caddr fn)) ) ;?
    ((eq (car fn) 'LAMBDA) (l15-eval (caddr fn)
                                     (nconc (l15-pair (cadr fn) args) a))) ;?
    (t (l15-apply (l15-eval fn a) args a)) )) ;symbol bound in alist?

(defun l15-eval (form a)
  (cond
    ((null form) nil)
    ((eq 'T form) 'T)
    ((numberp form) form)
    ((atom form)
     (cond
       ((get form 'APVAL) (get form 'APVAL))
       ((null (assoc form a)) (error "(EVAL) A8: UNBOUND VARIABLE")) ;?
       (t (cdr (assoc form a)))
       ))
    ((atom (car form))
     (cond
       ((get (car form) 'EXPR)
        (l15-apply (get (car form) 'EXPR) (l15-evlis (cdr form) a) a))
       ((get (car form) 'FEXPR)
        (l15-apply (get (car form) 'FEXPR) (list (cdr form) a) a))
       ; get FEXPR
       ((eq (car form) 'CSETQ) (l15-cset (cadr form) (l15-eval (caddr form) a)))
       ; get SUBR
       ((eq (car form) 'CAR) (car (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'CDR) (cdr (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'CONS) (let ((evlis (l15-evlis (cdr form) a)))
                                (cons (car evlis) (cadr evlis))))
       ((eq (car form) 'ATOM) (l15-atom (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'NULL) (null (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'NUMBERP) (numberp (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'PRINT) (format t "~a~%" (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'EQ) (let ((evlis (l15-evlis (cdr form) a)))
                                (l15-eq (car evlis) (cadr evlis))))
       ((eq (car form) 'EQUAL) (let ((evlis (l15-evlis (cdr form) a)))
                                (equal (car evlis) (cadr evlis))))
       ((eq (car form) 'DEFINE) (mapcar 'l15-define (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'DEFLIST) (let ((evlis (l15-evlis (cdr form) a)))
                                  (mapcar 'l15-deflist (car evlis)
                                         (make-list (length (car evlis))
                                                    :initial-element (cadr evlis)))))
       ((eq (car form) 'GET) (let ((evlis (l15-evlis (cdr form) a)))
                                (get (car evlis) (cadr evlis))))
       ((eq (car form) 'CSET) (let ((evlis (l15-evlis (cdr form) a)))
                                (l15-cset (car evlis) (cadr evlis))))
       ((eq (car form) 'SET) (let ((evlis (l15-evlis (cdr form) a)))
                                (l15-set (car evlis) (cadr evlis))))
       ((eq (car form) 'ERROR) (error "(EVAL) ERROR: ~S"
                                      (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'EVLIS) (let ((evlis (l15-evlis (cdr form) a)))
                                (l15-evlis (car evlis) (cadr evlis))))
       ((eq (car form) 'EVAL) (let ((evlis (l15-evlis (cdr form) a)))
                                (l15-eval (car evlis) (cadr evlis))))
       ((eq (car form) 'APPLY) (let ((evlis (l15-evlis (cdr form) a)))
                                 (l15-apply (car evlis) (cadr evlis)
                                            (caddr evlis))))
       ((eq (car form) 'NCONC) (let ((evlis (l15-evlis (cdr form) a)))
                                 (l15-nconc (car evlis) (cadr evlis))))
       ((eq (car form) 'RPLACD) (let ((evlis (l15-evlis (cdr form) a)))
                                 (l15-rplacd (car evlis) (cadr evlis))))
       ((eq (car form) 'APPEND) (let ((evlis (l15-evlis (cdr form) a)))
                                 (append (car evlis) (cadr evlis))))
       ((eq (car form) 'ZEROP) (zerop (car (l15-evlis (cdr form) a))))
       ((eq (car form) 'TIMES) (let ((evlis (l15-evlis (cdr form) a)))
                                 (* (car evlis) (cadr evlis))))
       ((eq (car form) 'SUB1) (- (car (l15-evlis (cdr form) a)) 1))
       ((eq (car form) 'ADD1) (+ (car (l15-evlis (cdr form) a)) 1))
        ;get FSUBR
       ((eq (car form) 'QUOTE) (cadr form))
       ((eq (car form) 'FUNCTION) (list 'FUNARG (cadr form) a)) ;?
       ((eq (car form) 'COND) (l15-evcon (cdr form) a)) ;?
       ((eq (car form) 'LIST) (l15-evlis (cdr form) a))
       ((eq (car form) 'OR) (l15-or (cdr form) a))
       ((eq (car form) 'AND) (l15-and (cdr form) a))
       ((eq (car form) 'PROG) (l15-prog (cadr form) (cddr form) a))
       ((eq (car form) 'SETQ) (l15-set (cadr form) (l15-eval (caddr form) a)))
       ((null (assoc (car form) a))
        (error "(EVAL) A9: FUNCTION OBJECT HAS NO DEFINITION"))
       (t (l15-eval (cons (cdr (assoc (car form) a)) (cdr form)) a)) ))
    (t (l15-apply (car form) (l15-evlis (cdr form) a) a))))

(defun l15-evcon (c a)
  (cond
    ((null c) (error "(EVCON) A3: CONDITIONAL UNSATISFIED"))
    ((l15-eval (caar c) a) (l15-eval (cadar c) a))
    (t (l15-evcon (cdr c) a)) ))

(defun l15-evlis (m a)
  (mapcar (LAMBDA (j) (l15-eval j a)) m))

(defun l15-pair (x y)
  (mapcar 'cons x y) )

(defun l15-eq (x y)
  (eq x y)
  )

(defun l15-equal (x y)
  (cond
    ((atom x)
     (cond
       ((atom y) (eq x y))
       (t nil)))
    ((l15-equal (car x) (car y)) (l15-equal (cdr x) (cdr y)))
    (t nil) ))

(defun l15-define (name-fn)
  (setf (get (car name-fn) 'PNAME) (car name-fn))
  (setf (get (car name-fn) 'EXPR) (car (cdr name-fn)))
  (car name-fn))

(defun l15-deflist (name-fn ind)
  (setf (get (car name-fn) 'PNAME) (car name-fn))
  (setf (get (car name-fn) ind) (car (cdr name-fn)))
  (car name-fn))

(defun l15-cset (n v)
  (cond
    ((atom n)
      (setf (get n 'PNAME) n)
      (setf (get n 'APVAL) v))
    (t (error "(CSET/CSETQ): VAR NAME NOT ATOM" )))
  )

(defun l15-set (n v)
  (cond
    ((atom n)
      (rplacd (assoc n (car *prog-vars*)) v))
    (t (error "(SET/SETQ): VAR NAME NOT ATOM" )))
  )

(defun l15-or (x a)
  (cond
    ((null x) nil)
    ((not (null (l15-eval (car x) a))) t)
    (t (l15-or (cdr x) a))
    )
  )

(defun l15-and (x a)
  (cond
    ((null x) t)
    ((null (l15-eval (car x) a)) nil)
    (t (l15-and (cdr x) a))
    )
  )

(defun l15-atom (x)
  (atom x)
  )

(defun l15-nconc (x y)
  (nconc x y)
  )

(defun l15-rplacd (x y)
  (rplacd x y)
  )

(defun l15-prog (v s a)
  (setq *prog-vars* (cons
                      (a-list-scope
                        (l15-pair v (make-list (length v)
                                               :initial-element nil)) a)
                      *prog-vars*))
  (setq *prog-golist* (cons s *prog-golist*))
  (l15-prog-eval s)
)

(defun l15-prog-eval (s)
  (cond
    ((null s) nil)
    ((atom (car s)) (l15-prog-eval (cdr s)))
    ((eq (caar s) 'GO) (l15-prog-eval (l15-go (cadar s) (car *prog-golist*))))
    ((eq (caar s) 'COND) (l15-prog-eval (cons (l15-prog-eval-cond (cdar s)) (cdr s))))
    ((eq (caar s) 'RETURN) (let ((res (l15-eval (cadar s) (car *prog-vars*))))
                             (setq *prog-vars* (cdr *prog-vars*))
                             (setq *prog-golist* (cdr *prog-golist*))
                             res))
    (t (progn
         (l15-eval (car s) (car *prog-vars*))
         (l15-prog-eval (cdr s)))))
  )

(defun l15-go (l s)
  (cond
    ((null s) (error "(INTER) A6: GO REFERS TO A POINT NOT LABELLED"))
    ((eq (car s) l) (cdr s))
    (t (l15-go l (cdr s))))
  )

(defun l15-prog-eval-cond (c)
  (cond
    ((null c) nil)
    ((l15-eval (caar c) (car *prog-vars*)) (cadar c))
    (t (l15-prog-eval-cond (cdr c))) )
  )

(defun a-list-scope (res a)
  (cond
    ((null a) res)
    ((null (assoc (caar a) res)) (a-list-scope (cons (car a) res) (cdr a)))
    (t (a-list-scope res (cdr a))))
  )

(format t "Running file: ~a~%" (cadr sb-ext:*posix-argv*))
(evalquote-loop (read-from-string
                  (concatenate 'string "(" (read-file "minihelp-functions.help") ")")))
(evalquote-loop (read-from-string
                  (concatenate 'string "(" (read-file (cadr sb-ext:*posix-argv*)) ")")))

